SpaceWorlds
===========

An [LD48](http://ludumdare.com) Jam entry.  

Theme: Connected Worlds

[Entry Page](http://www.ludumdare.com/compo/ludum-dare-30/?action=preview&uid=20410)

Binaries
--------

### [Ludum Dare Release](https://github.com/DavidTruby/SpaceWorlds/releases/tag/v1.0)

* [Windows x86](https://github.com/DavidTruby/SpaceWorlds/releases/download/v1.0/SpaceWorlds-windows-i686.zip)
* [OS/X x64](https://github.com/DavidTruby/SpaceWorlds/releases/download/v1.0/SpaceWorlds-linux-x86_64.tar.xz)
* [Linux x64](https://github.com/DavidTruby/SpaceWorlds/releases/download/v1.0/SpaceWorlds-osx-x86_64.tar.xz)

#### Requirements

##### OS/X & Linux

[libsfml2.1](http://sfml-dev.org)  
[libboost](http://boost.org)  

Build
-----

    git clone git@github.com:DavidTruby/SpaceWorlds.git
    cd SpaceWorlds
    git submodule init && git submodule update
    mkdir build
    cd !$
    cmake ..
    make

#### Requirements 

* C++14 compiler (MinGW on Windows)  
* [CMake](http://www.cmake.org/)  
* [libsfml2.1](http://sfml-dev.org)  
* [libboost](http://boost.org)  

Credits
-------

#### Programming 

David Truby ([@DavidTruby](http://twitter.com/DavidTruby))  
Richard Perry  
[Jamie Bayne](http://jamiebayne.co.uk) ([@quaIiaa](http://twitter.com/quaIiaa))  
[Alex Dixon](http://www.alexbrainbox.co.uk/) ([@Alexbrainbox](http://twitter.com/Alexbrainbox))  
[Chris Leonard](http://veltas.uwcs.co.uk/) ([@VeltasV](http://twitter.com/VeltasV))

#### Art

Richard Perry

#### Music and Sound

[Jamie Bayne](http://jamiebayne.co.uk) ([@quaIiaa](http://twitter.com/quaIiaa))
